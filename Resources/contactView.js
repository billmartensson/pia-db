var win = Ti.UI.currentWindow;
win.backgroundColor = "#ffffff";

// Skapa textfields för att mata in namn och telefonnummer
var nameTextField = Ti.UI.createTextField({
	top: 30,
	left: 10,
	width: 200,
	height: "auto",
	hintText: "Namn",
	autocorrect: false, 
	borderColor: "#000000"
});

win.add(nameTextField);

var phoneTextField = Ti.UI.createTextField({
	top: 70,
	left: 10,
	width: 200,
	height: "auto",
	hintText: "Telefonnummer",
	autocorrect: false, 
	borderColor: "#000000"
});

win.add(phoneTextField);

// Om wincontactId inte är tomt dvs idnummret som vi skickade med fönstret 
if(win.contactId != null)
{
	var db = Ti.Database.open("kontakter");
	
	// Hämta från databsen är rowid är idnummer som skickades med
	var rows = db.execute('SELECT rowid,name,phone_number FROM contacts WHERE rowid = '+win.contactId);
	
	while (rows.isValidRow())
	{
		Ti.API.info('Person ---> ROWID: ' + rows.fieldByName('rowid') + ', name:' + rows.field(1) + ', phone_number: ' + rows.fieldByName('phone_number'));
		// Sätt textfields värde till värde från databasen
		nameTextField.value = rows.fieldByName('name');
		phoneTextField.value = rows.fieldByName('phone_number');
		
		rows.next();
	}
	rows.close();
	
	db.close();
	
}

// Knapp för att spara
var saveButton = Ti.UI.createButton({
	title: "Spara",
	width: "auto",
	height: "auto",
	top: 100
});
 
saveButton.addEventListener("click", function(e) {
	// Om det skickats med idnummer så uppdatera annars skapa ny rad
	if(win.contactId == null)
	{
		var db = Ti.Database.open("kontakter");
		// Skapa rad i databasen. Sätt namn och telefonnumemr till dessa två värde
		db.execute('INSERT INTO contacts (name, phone_number) VALUES (?, ?)', nameTextField.value, phoneTextField.value);
		
		db.close();		
	} else {
		var db = Ti.Database.open("kontakter");
		// Uppdatera raden
		db.execute('UPDATE contacts SET name = ?, phone_number = ? WHERE rowid = '+win.contactId, nameTextField.value, phoneTextField.value);
		
		db.close();		
	}
	
	// Kör funktionen i contactList för att uppdatera tableview
	Ti.App.fireEvent("updateContactList");
	
	// Stäng fönstret
	Ti.App.mynav.close(win, {animated:true});
});

win.add(saveButton);
