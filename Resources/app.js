// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

// Öppna databasen. Finns den inte skapas den.
var db = Ti.Database.open("kontakter");
// Backa upp till iCloud
db.file.remoteBackup = true;

// Skapa tabellen i databasen om den inte redan finns
db.execute('CREATE TABLE IF NOT EXISTS contacts (name TEXT, phone_number TEXT)');

// Stäng databasen när den inte behövs mer
db.close();

// Skapa ett yttre fönster som ska innehålla vår navigering
var outerWindow = Titanium.UI.createWindow();

// Skapa fönstret som vi ska börja visa
var tableWindow = Titanium.UI.createWindow({
    url: "contactList.js", 
    title: 'Kontakter'
});

// Skapa navigeringen
var nav = Titanium.UI.iPhone.createNavigationGroup({
   window: tableWindow
});

// Spara navigeringen så vi kommer åt den överallt
Ti.App.mynav = nav;

// Lägg till navigeringen i yttre fönstret och öppna den.
outerWindow.add(nav);
outerWindow.open();